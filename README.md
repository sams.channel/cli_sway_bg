### cli\_sway\_bg
> Simple CLI application to set background when using [swaywm](https://swaywm.org).

##### How to use

> You will need to install a [D compiler](http://dlang.org/download.html), [make](https://www.gnu.org/software/make) utility
and [UPX](https://upx.github.io) executable compressor to build from repository source.

* On [Void Linux](https://voidlinux.org), you can install the following packages with [XBPS](https://github.com/void-linux/xbps) (where dmd is the official reference compiler for D):

<pre># xbps-install -S dmd make upx</pre>

##### Build from source and install
<pre>$ make</pre> or <pre>make gdc</pre> or <pre>make ldc</pre>
<pre># make install</pre>

##### Usage

<pre>$ cli_sway_bg</pre>

to create a symbolic link (~/swaycfg) to your Sway WM configuration.
This only needs to be done once.

<pre>$ cli_sway_bg bg=/path/to/wallpaper_or_background_image.jpg</pre>

Then press Mod+Shift+c to refresh Sway WM and, if valid, your specified wallpaper will now be set.
