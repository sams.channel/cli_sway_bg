/**
    * CLI application to set background/wallpaper on a system using swaywm.
    * Author: Sam St-Pettersen, 2023
    * Released under the MIT License
*/

import std.stdio;
import std.conv;
import std.file;
import std.array;
import std.algorithm;
import std.string;
import std.process;
import std.format;

int setBackground(string program, string bg, string cfg) {
    // Check that background file exists,
    // if not throw an error.
    if (!exists(bg)) {
        return displayError(program, format
        ("Background file does not exist: \"%s\"", bg));
    }

    // Read in file and set background on applicable line.
    // Push each line to _out array.
    string[] _out = new string[1];
    File f = File(cfg, "r");
    foreach (line; f.byLine()) {
        string l = line.to!string;
        if (startsWith(l, "output * bg")) {
            l = format("output * bg \"%s\" fill", bg);
        }
        _out ~= l;
    }

    f.close();

    // Remove file.
    remove(cfg);

    // Write file out line by line to replacement file.
    foreach (line; _out) {
        append(cfg, line ~ "\n");
    }

    writefln("Set background to:\n\"%s\".", bg);
    writeln("Press Mod+Shift+c to refresh swaywm settings.");

    return 0;
}

int displayError(string program, string err) {
    writefln("Error: %s.\n", err);
    return displayUsage(program, -1);
}

int displayUsage(string program, int exitCode) {
    writeln("CLI utility to set background/wallpaper on a system using swaywm.");
    writeln("Written by Sam St-Pettersen <s.stpettersen@pm.me>\n");
    writefln("Usage: %s OPTION\n", program);
    writeln("Options:");
    writeln("bg=PATH_TO_BG_IMAGE    Specify the background/wallpaper to set.\n");
    return exitCode;
}

int main(string[] args) {
    int exitCode = 0;
    immutable string program = "cli_sway_bg";

    version(Windows) {
        writeln("This program is only intended for Unix-like OSes.");
        return exitCode;
    }

    // Check swaywm is installed via detecting its configuration.
    string user = environment.get("USER");
    string cfg = format("/home/%s/.config/sway/config", user);

    if (!exists(cfg)) {
        writeln("Error: swaywm not detected.");
        writeln("Please install it and create your configuration");
        writeln("under ~/.config/sway/\n");
        exitCode = displayUsage(program, -1);
        return exitCode;
    }

    // Create a symbolic link (~./swaycfg) to swaywm main
    // configuration file if it does not already exist.
    string link = format("/home/%s/swaycfg", user);

    version(Posix) {
        if (!exists(link)) {
            symlink(cfg, link);
            writeln("Created a symbolic link to swaywm main config file:");
            writeln("~/swaycfg\n");
        }
    }

    if (args.length > 1) {
        foreach (a; args) {
            if (startsWith(a, "bg=")) {
                string bg = replace(a, "bg=", "");
                exitCode = setBackground(program, bg, cfg);
                return exitCode;
            }
            else if (!startsWith(a, program)) {
                exitCode = displayError(program, format("\"%s\" is not a valid option", a));
                return exitCode;
            }
        }
    }
    else {
        exitCode = displayUsage(program, 0);
    }

    return exitCode;
}
