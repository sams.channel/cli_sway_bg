TARGET=cli_sway_bg
SOURCE=cli_sway_bg.d
REF_COMPILER=dmd
GNU_COMPILER=gdc
LLVM_COMPILER=ldc2

make:
	$(REF_COMPILER) $(SOURCE)
	rm $(TARGET).o
	strip $(TARGET)

gdc:
	$(GNU_COMPILER) $(SOURCE) -o $(TARGET)
	strip $(TARGET)

ldc:
	$(LLVM_COMPILER) $(SOURCE)
	rm $(TARGET).o
	strip $(TARGET)

install:
	@echo Please run this as sudo/doas.
	upx -9 -o /usr/bin/$(TARGET) $(TARGET)

uninstall:
	@echo Please run this as sudo/doas.
	rm /usr/bin/$(TARGET)

clean:
	rm $(TARGET)
